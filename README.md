# NoSQL Fantasy

Para rodar o projeto em desenvolvimento

```bash
docker-compose build
docker-compose up -d
docker-compose exec api rails db:setup
```

Após terminar de desenvolver, rode:

```bash
docker-compose down
```

