# define imagem base para a nossa
FROM ruby:2.5-alpine

# determina o path dentro do conteiner
ENV API_PATH=/usr/src/labdb_api
WORKDIR $API_PATH

# define algumas variáveis úteis
ENV PORT=3000 \
    HOST=0.0.0.0 \
    RAILS_ENV=development

# instala dependências a nivel de SO
RUN apk add --update \
    build-base \
    postgresql-dev \
    tzdata \
    && rm -rf /var/cache/apk/*

# copia arquivos de gerenciamento de dependências internas
COPY Gemfile Gemfile.lock ./
RUN bundle install

# abre a porta
EXPOSE $PORT

# definindo o comando padrão
CMD sh
