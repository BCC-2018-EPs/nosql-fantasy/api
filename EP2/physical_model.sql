--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
------------------------------------CREATION------------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

CREATE SCHEMA nosequel_fantasy;

SET search_path TO nosequel_fantasy;

CREATE TABLE locations (
    id serial,
    sprite text NOT NULL,    
    name text NOT NULL,
    CONSTRAINT locations_pkey
        PRIMARY KEY (id)
);

CREATE TABLE buildings (
    id serial,
    location_id integer NOT NULL,
    sprite text NOT NULL,    
    name text NOT NULL,
    CONSTRAINT buildings_pkey
        PRIMARY KEY (id),
    CONSTRAINT buildings_fkey_locations
        FOREIGN KEY (location_id) REFERENCES locations(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE inns (
    building_id integer NOT NULL,
    cost integer NOT NULL,
    CONSTRAINT inns_pkey
        PRIMARY KEY (building_id),
    CONSTRAINT inns_fkey_buildings
        FOREIGN KEY (building_id) REFERENCES buildings(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE shops (
    building_id integer NOT NULL,
    CONSTRAINT shops_pkey
        PRIMARY KEY (building_id),
    CONSTRAINT shops_fkey_buildings
        FOREIGN KEY (building_id) REFERENCES buildings(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE paths (
    departure integer NOT NULL,
    destination integer NOT NULL,    
    length integer NOT NULL,
    CONSTRAINT paths_pkey
        PRIMARY KEY (departure, destination),
    CONSTRAINT paths_fkey_locations_departure
        FOREIGN KEY (departure) REFERENCES locations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT paths_fkey_locations_destination
        FOREIGN KEY (destination) REFERENCES locations(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE specials (
    id serial,
    target integer NOT NULL,
    power integer NOT NULL,
    stat integer NOT NULL,
    CONSTRAINT specials_pkey
        PRIMARY KEY (id)
);

CREATE TABLE items (
    name text NOT NULL,
    cost integer NOT NULL,
    CONSTRAINT items_pkey
        PRIMARY KEY (name)
);

CREATE TABLE rel_items_shops (
    item_name text NOT NULL,
    shop_building_id integer NOT NULL,
    CONSTRAINT rel_items_shops_pkey
        PRIMARY KEY (item_name, shop_building_id),
    CONSTRAINT rel_items_shops_fkey_items
        FOREIGN KEY (item_name) REFERENCES items(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT rel_items_shops_fkey_shops
        FOREIGN KEY (shop_building_id) REFERENCES shops(building_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE consumables (
    item_name text NOT NULL,
    special_id integer NOT NULL,
    CONSTRAINT consumables_pkey
        PRIMARY KEY (item_name, special_id),
    CONSTRAINT consumables_fkey_items
        FOREIGN KEY (item_name) REFERENCES items(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT consumables_fkey_specials
        FOREIGN KEY (special_id) REFERENCES specials(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE equipments (
    item_name text NOT NULL,
    type integer NOT NULL,
    CONSTRAINT equipments_pkey
        PRIMARY KEY (item_name)
);

CREATE TABLE bonuses (
    id serial,
    equipment_item_name text NOT NULL,
    stat text NOT NULL,
    amount integer NOT NULL,
    CONSTRAINT bonuses_pkey
        PRIMARY KEY (id),
    CONSTRAINT bonuses_fkey_equipments
        FOREIGN KEY (equipment_item_name) REFERENCES equipments(item_name) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE skills (
    name text NOT NULL,
    special_id integer NOT NULL,
    accuracy numeric (1,2) NOT NULL,
    cost integer NOT NULL,
    multiplier_type integer NOT NULL,
    CONSTRAINT skills_pkey
        PRIMARY KEY (name),
    CONSTRAINT skills_fkey_specials
        FOREIGN KEY (special_id) REFERENCES specials(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT valid_accuracy
        CHECK (accuracy >= 0 AND accuracy <= 1)
);

CREATE TABLE stats (
    id serial,
    hp integer NOT NULL,
    mp integer NOT NULL,
    ap integer NOT NULL,
    str integer NOT NULL,
    agi integer NOT NULL,
    dex integer NOT NULL,
    int integer NOT NULL,
    def integer NOT NULL,
    luk integer NOT NULL,
    CONSTRAINT stats_pkey
        PRIMARY KEY (id)
);

CREATE TABLE classes (
    name text NOT NULL,
    base integer NOT NULL,
    growth integer NOT NULL,
    CONSTRAINT classes_pkey
        PRIMARY KEY (name),
    CONSTRAINT classes_fkey_stats_base
        FOREIGN KEY (base) REFERENCES stats(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT classes_fkey_stats_growth
        FOREIGN KEY (growth) REFERENCES stats(id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE rel_classes_skills (
    class_name text NOT NULL,
    skill_name text NOT NULL,
    level integer NOT NULL,
    CONSTRAINT rel_classes_skills_pkey
        PRIMARY KEY (class_name, skill_name),
    CONSTRAINT rel_classes_skills_fkey_classes
        FOREIGN KEY (class_name) REFERENCES classes(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT rel_classes_skills_fkey_skills
        FOREIGN KEY (skill_name) REFERENCES skills(name) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE monsters (
    name text NOT NULL,
    sprite text NOT NULL,
    stats_id integer NOT NULL,
    experience integer NOT NULL,
    CONSTRAINT monsters_pkey
        PRIMARY KEY (name),
    CONSTRAINT monsters_fkey_stats
        FOREIGN KEY (stats_id) REFERENCES stats(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE rel_items_monsters (
    item_name text NOT NULL,
    monster_name text NOT NULL,
    chance numeric (1,2) NOT NULL,
    CONSTRAINT rel_items_monsters_pkey
        PRIMARY KEY (item_name, monster_name),
    CONSTRAINT rel_items_monsters_fkey_items
        FOREIGN KEY (item_name) REFERENCES items(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT rel_items_monsters_fkey_monsters
        FOREIGN KEY (monster_name) REFERENCES monsters(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT valid_chance
        CHECK (chance > 0 AND chance <= 1)
);

CREATE TABLE rel_monsters_paths (
    path_departure integer NOT NULL,
    path_destination integer NOT NULL,    
    monster_name text NOT NULL,
    lvl_min integer NOT NULL,
    lvl_max integer NOT NULL,
    factor numeric (2,2) NOT NULL,
    CONSTRAINT rel_monsters_paths_pkey
        PRIMARY KEY (path_departure, path_destination, monster_name),
    CONSTRAINT rel_monsters_paths_fkey_monsters
        FOREIGN KEY (monster_name) REFERENCES monsters(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT rel_monsters_paths_fkey_paths
        FOREIGN KEY (path_departure, path_destination) REFERENCES paths(departure, destination) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE rel_monsters_skills (
    monster_name text NOT NULL,
    skill_name text NOT NULL,
    CONSTRAINT rel_monsters_skills_pkey
        PRIMARY KEY (monster_name, skill_name),
    CONSTRAINT rel_monsters_skills_fkey_monsters
        FOREIGN KEY (monster_name) REFERENCES monsters(name) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT rel_monsters_skills_fkey_skills
        FOREIGN KEY (skill_name) REFERENCES skills(name) ON UPDATE CASCADE ON DELETE CASCADE
);
