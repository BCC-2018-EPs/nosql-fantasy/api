db.users.insertMany(
	[
		{
			login: 'user1',
			password: '0B14D501A594442A01C6859541BCB3E8164D183D32937B851835442F69D5C94E',
			characters: [
				{
					name: 'Player 1',
					class: 'archer',
					level: 15,
					experience: 2000,
					gold: 500,
					current_hp: 200,
					current_mp: 50,
					current_ap: 150,
					current_location: 1,
					stats: {
						hp: 200,
						mp: 100,
						ap: 200,
						str: 17,
						agi: 35,
						dex: 50,
						int: 7,
						def: 9,
						luk: 21
					},
					skills: [
						{
							name: 'Double Strafe',
							accuracy: 1.00,
							cost: 2,
							multiplier_type: 5,
							special: {
								power: 0,
								target: 25,
								stat: 0 
							}
						},
						{
							name: 'Arrow Shower',
							accuracy: 1.00,
							cost: 10,
							multiplier_type: 5,
							special: {
								power: 0,
								target: 45,
								stat: 0 
							}
						},
					],
					items: [
						{
							name: 'Relational bow',
							cost: 270,
							equipment: {
								type: 3,
								bonuses: [
									{
										stat: 2,
										amount: 10 
									},
									{
										stat: 4,
										amount: 5 
									}
								]
							}
						},
						{
							name: 'Relational armor',
							cost: 270,
							equipment: {
								type: 3,
								bonuses: [
									{
										stat: 7,
										amount: 60 
									}
								]
							}
						},
						{
							name: 'Select potion',
							cost: 15,
							special: {
								power: 0,
								target: 10,
								stat: 0
							}
						}
					],
					current_equipments: {
						helmet: '',
						armor: 'Relational armor',
						legs: '',
						feet: '',
						accessory: '',
						left_hand: 'Relational bow',
						right_hand: 'Relational bow',
						quiver: ''
					},
					log: {
						events: [
							{
								type: 'Start',
								descriptions: 'Beginning of the game'
							},
							{
								type: 'Quest',
								descriptions: 'First quest'
							}
						],
						movement: [
							{
								path: {
									departure: 1,
									destination: 2,
									length: 5
								},
								battles: [
									{
										monster_name: 'Rat',
										experience: 10,
										result: 'win',
										item_drops: [
											{
												item_name: 'Select potion'
											}
										]
									}
								]
							}
						]
					}
				}
			]
		},
		{
			login: 'user2',
			password: '0B14D501A594442A01C6859541BCB3E8164D183D32937B851835442F69D5C94E',
			characters: [
				{
					name: 'Player 2',
					class: 'knight',
					level: 30,
					experience: 14200,
					gold: 3100,
					current_hp: 1200,
					current_mp: 250,
					current_ap: 650,
					current_location: 3,
					stats: {
						hp: 1500,
						mp: 300,
						ap: 800,
						str: 77,
						agi: 25,
						dex: 30,
						int: 9,
						def: 69,
						luk: 11
					},
					skills: [
						{
							name: 'Bash',
							accuracy: 1.00,
							cost: 3,
							multiplier_type: 3,
							special: {
								power: 0,
								target: 25,
								stat: 0 
							}
						},
						{
							name: 'Pierce',
							accuracy: 0.90,
							cost: 5,
							multiplier_type: 3,
							special: {
								power: 0,
								target: 25,
								stat: 0 
							}
						},
					],
					items: [
						{
							name: 'Query sword',
							cost: 230,
							equipment: {
								type: 3,
								bonuses: [
									{
										stat: 2,
										amount: 10 
									},
									{
										stat: 4,
										amount: 5 
									}
								]
								}
						},
						{
							name: 'Query shield',
							cost: 180,
							equipment: {
								type: 1,
								bonuses: [
									{
										stat: 7,
										amount: 60 
									}
								]
							}
						},
						{
							name: 'Select potion',
							cost: 15,
							special: {
								power: 10,
								target: 0,
								stat: 0
							}
						},
						{
							name: 'Join potion',
							cost: 30,
							special: {
								power: 25,
								target: 0,
								stat: 0
							}
						},
						{
							name: 'Query armor',
							cost: 270,
							equipment: {
								type: 1,
								bonuses: [
									{
										stat: 7,
										amount: 90 
									}
								]
							}
						},
						{
							name: 'Query pants',
							cost: 240,
							equipment: {
								type: 1,
								bonuses: [
									{
										stat: 7,
										amount: 75 
									}
								]
							}
						}
					],
					current_equipments: {
						helmet: 'Query helmet',
						armor: 'Query armor',
						legs: 'Query pants',
						feet: '',
						accessory: '',
						left_hand: 'Query shield',
						right_hand: 'Query sword',
						quiver: ''
					},
					log: {
						events: [
							{
								type: 'Start',
								descriptions: 'Beginning of the game'
							},
							{
								type: 'Quest',
								descriptions: 'First quest'
							},
							{
								type: 'Quest',
								descriptions: 'Second quest'
							},
							{
								type: 'Quest',
								descriptions: 'Third quest'
							}
						],
						movement: [
							{
								path: {
									departure: 1,
									destination: 2,
									length: 5
								},
								battles: [
									{
										monster_name: 'Rat',
										experience: 10,
										result: 'win',
										item_drops: [
											{
												item_name: 'Select potion'
											}
										]
									},
									{
										monster_name: 'Rat',
										experience: 10,
										result: 'win',
									}
								]
							},
							{
								path: {
									departure: 2,
									destination: 3,
									length: 10
								},
								battles: [
									{
										monster_name: 'Wold',
										experience: 20,
										result: 'win',
										item_drops: [
											{
												item_name: 'Join potion'
											}
										]
									}
								]
							}
						]
					}
				}
			]
		}
	]
);
=======
  [
    {
      login: 'user1',
      password: '0B14D501A594442A01C6859541BCB3E8164D183D32937B851835442F69D5C94E',
      characters: [
        {
          name: 'Player 1',
          class: 'archer',
          level: 15,
          experience: 2000,
          gold: 500,
          current_hp: 200,
          current_mp: 50,
          current_ap: 150,
          current_location: 1,
          stats: {
            hp: 200,
            mp: 100,
            ap: 200,
            str: 17,
            agi: 35,
            dex: 50,
            int: 7,
            def: 9,
            luk: 21
          },
          skills: [
            {
              name: 'Double Strafe',
              accuracy: 1.00,
              cost: 2,
              multiplier_type: 5,
              special: {
                power: 0,
                target: 25,
                stat: 0 
              }
            },
            {
              name: 'Arrow Shower',
              accuracy: 1.00,
              cost: 10,
              multiplier_type: 5,
              special: {
                power: 0,
                target: 45,
                stat: 0 
              }
            },
          ],
          items: [
            {
              name: 'Relational bow',
              cost: 270,
              equipment: {
                type: 3,
                bonuses: [
                  {
                    stat: 2,
                    amount: 10 
                  },
                  {
                    stat: 4,
                    amount: 5 
                  }
                ]
              }
            },
            {
              name: 'Relational armor',
              cost: 270,
              equipment: {
                type: 3,
                bonuses: [
                  {
                    stat: 7,
                    amount: 60 
                  }
                ]
              }
            },
            {
              name: 'Select potion',
              cost: 15,
              special: {
                power: 0,
                target: 10,
                stat: 0
              }
            }
          ],
          current_equipments: {
            helmet: '',
            armor: 'Relational armor',
            legs: '',
            feet: '',
            accessory: '',
            left_hand: 'Relational bow',
            right_hand: 'Relational bow',
            quiver: ''
          },
          log: {
            events: [
              {
                type: 'Start',
                descriptions: 'Beginning of the game'
              },
              {
                type: 'Quest',
                descriptions: 'First quest'
              }
            ],
            movement: [
              {
                path: {
                  departure: 1,
                  destination: 2,
                  length: 5
                },
                battles: [
                  {
                    monster_name: 'Rat',
                    experience: 10,
                    result: 'win',
                    item_drops: [
                      {
                        item_name: 'Select potion'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        }
      ]
    },
    {
      login: 'user2',
      password: '0B14D501A594442A01C6859541BCB3E8164D183D32937B851835442F69D5C94E',
      characters: [
        {
          name: 'Player 2',
          class: 'knight',
          level: 30,
          experience: 14200,
          gold: 3100,
          current_hp: 1200,
          current_mp: 250,
          current_ap: 650,
          current_location: 3,
          stats: {
            hp: 1500,
            mp: 300,
            ap: 800,
            str: 77,
            agi: 25,
            dex: 30,
            int: 9,
            def: 69,
            luk: 11
          },
          skills: [
            {
              name: 'Bash',
              accuracy: 1.00,
              cost: 3,
              multiplier_type: 3,
              special: {
                power: 0,
                target: 25,
                stat: 0 
              }
            },
            {
              name: 'Pierce',
              accuracy: 0.90,
              cost: 5,
              multiplier_type: 3,
              special: {
                power: 0,
                target: 25,
                stat: 0 
              }
            },
          ],
          items: [
            {
              name: 'Query sword',
              cost: 230,
              equipment: {
                type: 3,
                bonuses: [
                  {
                    stat: 2,
                    amount: 10 
                  },
                  {
                    stat: 4,
                    amount: 5 
                  }
                ]
                }
            },
            {
              name: 'Query shield',
              cost: 180,
              equipment: {
                type: 1,
                bonuses: [
                  {
                    stat: 7,
                    amount: 60 
                  }
                ]
              }
            },
            {
              name: 'Select potion',
              cost: 15,
              special: {
                power: 10,
                target: 0,
                stat: 0
              }
            },
            {
              name: 'Join potion',
              cost: 30,
              special: {
                power: 25,
                target: 0,
                stat: 0
              }
            },
            {
              name: 'Query armor',
              cost: 270,
              equipment: {
                type: 1,
                bonuses: [
                  {
                    stat: 7,
                    amount: 90 
                  }
                ]
              }
            },
            {
              name: 'Query pants',
              cost: 240,
              equipment: {
                type: 1,
                bonuses: [
                  {
                    stat: 7,
                    amount: 75 
                  }
                ]
              }
            }
          ],
          current_equipments: {
            helmet: 'Query helmet',
            armor: 'Query armor',
            legs: 'Query pants',
            feet: '',
            accessory: '',
            left_hand: 'Query shield',
            right_hand: 'Query sword',
            quiver: ''
          },
          log: {
            events: [
              {
                type: 'Start',
                descriptions: 'Beginning of the game'
              },
              {
                type: 'Quest',
                descriptions: 'First quest'
              },
              {
                type: 'Quest',
                descriptions: 'Second quest'
              },
              {
                type: 'Quest',
                descriptions: 'Third quest'
              }
            ],
            movement: [
              {
                path: {
                  departure: 1,
                  destination: 2,
                  length: 5
                },
                battles: [
                  {
                    monster_name: 'Rat',
                    experience: 10,
                    result: 'win',
                    item_drops: [
                      {
                        item_name: 'Select potion'
                      }
                    ]
                  },
                  {
                    monster_name: 'Rat',
                    experience: 10,
                    result: 'win',
                  }
                ]
              },
              {
                path: {
                  departure: 2,
                  destination: 3,
                  length: 10
                },
                battles: [
                  {
                    monster_name: 'Wold',
                    experience: 20,
                    result: 'win',
                    item_drops: [
                      {
                        item_name: 'Join potion'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        }
      ]
    }
  ]
);
