--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-----------------------------------INSERTIONS-----------------------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

INSERT INTO locations (sprite, name) VALUES
('locations/mongo_land.jpg', 'Mongo Land'),
('locations/postgre_ville.jpg', 'Postgre Ville'),
('locations/neo_town.jpg', 'Neo Town');

INSERT INTO buildings (location_id, sprite, name) VALUES
(1, 'buildings/mongo_land/inn.jpg', 'Aggregation Inn'),
(1, 'buildings/mongo_land/equip_shop.jpg', 'Schemaless Equips'),
(1, 'buildings/mongo_land/item_shop.jpg', 'Scalable Items'),
(2, 'buildings/postgre_ville/inn.jpg', 'Relational Inn'),
(3, 'buildings/neo_town/inn.jpg', 'Vertex Inn'),
(3, 'buildings/neo_town/item_shop.jpg', 'Directional Items');

INSERT INTO inns (building_id, cost) VALUES 
(1, 100),
(4, 200),
(5, 300);

INSERT INTO shops (building_id) VALUES 
(2),
(3),
(6);

INSERT INTO paths (departure, destination, length) VALUES 
(1, 2, 5),
(2, 1, 5),
(2, 3, 10),
(3, 2, 10);

INSERT INTO specials (target, power, stat) VALUES 
(0, 10, 0),
(0, 20, 0),
(0, 50, 0),
(0, 15, 0),
(0, 25, 0),
(0, 45, 0),
(0, 10, 0),
(0, 30, 0);

INSERT INTO items (name, cost) VALUES
('Relational dagger', 75),
('Relational bow', 80),
('Relational sword', 115),
('Relational long sword', 130),
('Relational shield', 90),
('Relational helmet', 75),
('Relational armor', 135),
('Relational pants', 120),
('Query dagger', 150),
('Query bow', 190),
('Query sword', 230),
('Query long sword', 260),
('Query shield', 180),
('Query helmet', 150),
('Query armor', 270),
('Query pants', 240),
('Select potion', 15),
('Join potion', 30);

INSERT INTO rel_items_shops (item_name, shop_building_id) VALUES
('Relational sword', 3),
('Relational bow', 3),
('Relational dagger', 3),
('Relational long sword', 3),
('Relational shield', 3),
('Relational helmet', 3),
('Relational armor', 3),
('Relational pants', 3),
('Query bow', 3),
('Query dagger', 3),
('Query sword', 3),
('Query long sword', 3),
('Query shield', 3),
('Query helmet', 3),
('Query armor', 3),
('Query pants', 3);

INSERT INTO consumables (item_name, special_id) VALUES
('Select potion', 1),
('Join potion', 5);

INSERT INTO equipments (item_name, type) VALUES
('Relational dagger',0),
('Relational sword', 0),
('Relational long sword', 0),
('Relational shield', 1),
('Relational helmet', 2),
('Relational armor', 3),
('Relational pants', 4),
('Query dagger', 0),
('Query sword', 0),
('Query long sword', 0),
('Query shield', 1),
('Query helmet', 2),
('Query armor', 3),
('Query pants', 4);

INSERT INTO bonuses (equipment_item_name, stat, amount) VALUES
('Relational dagger', 2, 10),
('Relational bow', 2, 10),
('Relational bow', 4, 5),
('Relational sword', 2, 10),
('Relational sword', 4, 5),
('Relational long sword', 2, 30),
('Relational long sword', 3, 10),
('Relational long sword', 4, -5),
('Relational shield', 7, 40),
('Relational helmet', 7, 25),
('Relational armor', 7, 60),
('Relational pants', 7, 50),
('Query dagger', 2, 15),
('Query sword', 2, 15),
('Query sword', 4, 8),
('Query long sword', 2, 45),
('Query long sword', 3, 15),
('Query long sword', 4, -8),
('Query shield', 7, 60),
('Query helmet', 7, 38),
('Query armor', 7, 90),
('Query pants', 7, 75);

INSERT INTO skills (name, special_id, accuracy, cost, multiplier_type) VALUES
('Bash', 1, 1.0, 3, 3),
('Pierce', 1, 0.9, 5, 3),
('Wild Swing', 2, 0.5, 8, 3),
('Scorch', 3, 1.0, 5, 6),
('Spark', 4, 0.95, 7, 6),
('Fireball', 5, 1.00, 11, 6),
('Double Strafe', 6, 1.00, 2, 5),
('Arrow Shower', 7, 1.00, 10, 5),
('Tackle', 8, 1.00, 0, 3),
('Bite', 9, 1.00, 1, 3);

INSERT INTO stats (hp, mp, ap, str, agi, dex, int, def, luk) VALUES
(30, 12, 18,  16,   8,  10,   8,  20,   6), --- kni base
(12, 30, 12,  10,  10,   8,   8,  12,  12), --- wiz base
(12, 30, 12,   9,  12,  12,  16,  12,  18), --- arc base
(15,  6,  9,   8,   4,   5,   4,  10,   3), --- kni growth
( 6, 15,  6,   5,   5,   4,   4,   6,   6), --- wiz growth
( 6, 15,  6,   5,   6,   6,   8,   6,   9), --- arc growth
( 3,  0,  4,   1,   2,   3,   1,   1,   3), --- rat
( 6,  2,  6,   5,   1,   4,   2,   2,   4), --- wolf
( 9,  5,  8,   6,   5,   8,   3,   7,   4); --- alpha wolf

INSERT INTO classes (name, base, growth) VALUES
('knight', 1, 4),
('wizard', 2, 5),
('archer', 3, 6);

INSERT INTO rel_classes_skills (class_name, skill_name, level) VALUES 
('knight', 'Bash'),
('knight', 'Pierce'),
('knight', 'Wild swing'),
('wizard', 'Scorch'),
('wizard', 'Spark'),
('wizard', 'Fireball'),
('archer', 'Double Strafe'),
('archer', 'Arrow Shower');

INSERT INTO monsters (name, sprite, stats_id, experience) VALUES 
('Rat', 'monsters/rat.png', 7, 10)
('Wolf', 'monsters/wolf.png', 8, 20)
('Alpha wolf', 'monsters/wolf.png', 9, 40);

INSERT INTO rel_items_monsters (item_name, monster_name, chance) VALUES 
('Rat', 'Select potion', 0.2),
('Wolf', 'Select potion', 0.3),
('Wolf', 'Join potion', 0.2),
('Alpha wolf', 'Join potion', 0.4),

INSERT INTO rel_monsters_paths (path_departure, path_destination, monster_name, lvl_min, lvl_max, factor) VALUES 
(1, 2, 'Rat', 1, 5, 5);
(2, 1, 'Rat', 1, 5, 5);
(1, 2, 'Wolf', 3, 10, 1);
(2, 1, 'Wolf', 1, 5, 1);
(2, 3, 'Wolf', , 5, 1);

INSERT INTO rel_monsters_skills (monster_name, skill_name) VALUES 
('Rat', 'Tackle'),
('Wolf', 'Tackle'),
('Alpha wolf', 'Tackle'),
('Alpha wolf', 'Bite');