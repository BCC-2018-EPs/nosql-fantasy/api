--- queries all monsters that live in a given path
CREATE OR REPLACE FUNCTION monsters_of_path(
    IN given_departure INT,
    IN given_destination INT,
    IN char_level INT
) RETURNS monsters AS $$
    SELECT M.*
    FROM monsters AS M
    LEFT JOIN rel_monsters_paths AS RMP
    ON M.name = RMP.monster_name
    WHERE RMP.path_departure = given_departure
    AND RMP.path_destination = given_destination
    AND RMP.lvl_min <= char_level
    AND char_level <= RMP.lvl_max;
$$ LANGUAGE sql;

--- queries all items sold in a given shop
CREATE OR REPLACE FUNCTION items_on_shop(
    IN given_shop INT
) RETURNS SETOF items AS $$
    SELECT I.*
    FROM items AS I
    LEFT JOIN rel_items_shops AS RIS
    ON RIS.item_name = I.name
    WHERE shop_building_id = given_shop;
$$ LANGUAGE sql;

--- queries the base stat for a given class
CREATE OR REPLACE FUNCTION class_base_stats(
    IN given_class_name TEXT
) RETURNS stats AS $$
    SELECT S.*
    FROM stats AS s
    LEFT JOIN classes AS C
    ON C.base = S.id
    WHERE C.name = given_class_name;
$$ LANGUAGE sql;

