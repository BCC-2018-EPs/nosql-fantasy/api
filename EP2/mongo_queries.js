// get all characters in location 1
db.users.find(
	{
		"characters.current_location": 1
	}
);

// get the current location of the characters with level greater than 10
db.users.find(
	{
		"characters.level": {$gte: 10}
	},
	{
		"characters.current_location": 1
	}
);

// get the characters from the user "user1"
db.users.find(
	{
		"login": "user1"
	},
	{
		"characters": 1,
	}
);

// get the skills of the character "Player 1"
db.users.find(
	{
		"characters.name": "Player 1"
	},
	{
		"characters.skills": 1
	}
);

// get the gold, inventory and current equipment of the character "Player 2"
db.users.find(
	{
		"characters.name": "Player 2"
	},
	{
		"characters.gold": 1,
		"characters.current_equipments": 1,
		"characters.items": 1
	}
);