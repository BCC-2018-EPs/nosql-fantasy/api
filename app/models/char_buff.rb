class CharBuff
  include Mongoid::Document
  field :stat, type: Integer
  field :amount, type: Integer

  embedded_in :char_equipment
end
