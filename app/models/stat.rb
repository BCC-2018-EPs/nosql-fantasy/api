class Stat < ApplicationRecord
  [:hp, :mp, :ap, :str, :agi, :dex, :int, :def, :luk].each do |attr|
    validates attr, numericality: {greater_than: -1}
  end

  # validate uniqueness of the whole set
end
