class User
  include Mongoid::Document
  field :login, type: String
  field :password, type: String

  embeds_many :characters
end
