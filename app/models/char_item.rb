class CharItem
  include Mongoid::Document
  field :name, type: String
  field :cost, type: Integer

  embedded_in :character
  embeds_one :item_special, class_name: 'CharSpecial'
  embedded_in :helmet, class_name: 'CharCurrentEquipment', inverse_of: :helmet
  embedded_in :torso, class_name: 'CharCurrentEquipment', inverse_of: :torso
  embedded_in :legs, class_name: 'CharCurrentEquipment', inverse_of: :legs
  embedded_in :feet, class_name: 'CharCurrentEquipment', inverse_of: :feet
  embedded_in :accessory, class_name: 'CharCurrentEquipment', inverse_of: :accessory
  embedded_in :left_hand, class_name: 'CharCurrentEquipment', inverse_of: :left_hand
  embedded_in :right_hand, class_name: 'CharCurrentEquipment', inverse_of: :right_hand
  embedded_in :quiver, class_name: 'CharCurrentEquipment', inverse_of: :quiver
  embeds_one :char_equipment

end
