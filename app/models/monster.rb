class Monster < ApplicationRecord
  belongs_to :stat
  has_many :rel_monster_paths
  has_many :paths, through: :rel_monster_paths
  has_many :rel_item_monsters
  has_many :items, through: :rel_item_monsters
  has_many :rel_monster_skills
  has_many :skills, through: :rel_monster_skills


  validates :name, uniqueness: true
end
