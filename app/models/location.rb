class Location < ApplicationRecord
  has_many :exits, foreign_key: 'departure_id', class_name: 'Path'
  has_many :entrances, foreign_key: 'destination_id', class_name: 'Path'

  validates :name, uniqueness: true
end
