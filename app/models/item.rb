class Item < ApplicationRecord
  has_many :rel_item_monsters
  has_many :monsters, through: :rel_item_monsters
  has_one :consumable
  has_one :equipment

  validates :name, uniqueness: true
end
