class Effect < ApplicationRecord
  validates :name, uniqueness: true
  has_many :rel_effect_special
  has_many :special, through: :rel_effect_special
end
