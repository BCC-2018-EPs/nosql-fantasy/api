class CharStat
  include Mongoid::Document
  field :hp, type: Integer
  field :mp, type: Integer
  field :ap, type: Integer
  field :str, type: Integer
  field :agi, type: Integer
  field :dex, type: Integer
  field :int, type: Integer
  field :def, type: Integer
  field :luk, type: Integer

  embedded_in :character
end
