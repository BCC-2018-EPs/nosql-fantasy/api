class Character
  include Mongoid::Document
  field :name, type: String
  field :game_class_id, type: Integer
  field :level, type: Integer
  field :gold, type: Integer
  field :exp, type: Integer
  field :curr_hp, type: Integer
  field :curr_mp, type: Integer
  field :curr_ap, type: Integer
  field :curr_location_id, type: Integer

  embedded_in :user
  embeds_one :char_stat
  embeds_many :char_items
  embeds_many :char_skills
  embeds_one :char_current_equipment

  def stats
    self.char_stat
  end

  def bag
    self.char_items
  end

  def skills
    self.char_skills
  end
end