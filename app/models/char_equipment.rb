class CharEquipment
  include Mongoid::Document
  field :type, type: Integer

  embedded_in :char_item
  embeds_many :char_buffs
end
