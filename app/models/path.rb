class Path < ApplicationRecord
  belongs_to :departure, class_name: 'Location'
  belongs_to :destination, class_name: 'Location'
  has_many :rel_monster_paths
  has_many :monsters, through: :rel_monster_paths

  # validate uniqueness of the pair
  validates :length, numericality: {greater_than: 0}
end
