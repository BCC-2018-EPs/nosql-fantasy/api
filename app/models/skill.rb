class Skill < ApplicationRecord
  belongs_to :special
  has_many :rel_monster_skills
  has_many :monsters, through: :rel_monster_skills
  has_many :rel_class_skills
  has_many :classes, through: :rel_class_skills

  validates :name, uniqueness: true
  [:accuracy, :cost_type, :cost_amount, :ref_stat].each do |attr|
    validates attr, numericality: {greater_than: -1}
  end
end
