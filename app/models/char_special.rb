class CharSpecial
  include Mongoid::Document
  field :power, type: Integer
  field :target_self, type: Mongoid::Boolean
  field :stat, type: Integer

  embedded_in :char_skill, inverse_of: :skill_special
  embedded_in :char_item, inverse_of: :item_special
end
