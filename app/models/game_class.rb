class GameClass < ApplicationRecord
  belongs_to :base, class_name: 'Stat'
  belongs_to :growth, class_name: 'Stat'
  has_many :rel_class_skills
  has_many :skills, through: :rel_class_skills

  validates :name, uniqueness: true
end
