class CharSkill
  include Mongoid::Document
  field :name, type: String
  field :accuracy, type: Float
  field :cost_amount, type: Integer
  field :cost_type, type: Integer
  field :ref_stat, type: Integer

  embedded_in :character
  embeds_one :skill_special, class_name: 'CharSpecial'
end
