class Special < ApplicationRecord
  has_one :rel_effect_special
  has_one :effect, through: :rel_effect_special
  has_one :skill
  has_one :consumable
end
