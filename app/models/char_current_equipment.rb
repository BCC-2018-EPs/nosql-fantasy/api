class CharCurrentEquipment
  include Mongoid::Document

  embedded_in :character

  embeds_one :helmet, class_name: 'CharItem', inverse_of: :helmet
  embeds_one :torso, class_name: 'CharItem', inverse_of: :torso
  embeds_one :legs, class_name: 'CharItem', inverse_of: :legs
  embeds_one :feet, class_name: 'CharItem', inverse_of: :feet
  embeds_one :accessory, class_name: 'CharItem', inverse_of: :accessory
  embeds_one :left_hand, class_name: 'CharItem', inverse_of: :left_hand
  embeds_one :right_hand, class_name: 'CharItem', inverse_of: :right_hand
  embeds_one :quiver, class_name: 'CharItem', inverse_of: :quiver
end
