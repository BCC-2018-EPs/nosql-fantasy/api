class CharEquipmentsController < ApplicationController
  before_action :set_char_equipment, only: [:show, :update, :destroy]

  # GET /char_equipments
  def index
    @char_equipments = CharEquipment.all

    render json: @char_equipments
  end

  # GET /char_equipments/1
  def show
    render json: @char_equipment
  end

  # POST /char_equipments
  def create
    @char_equipment = CharEquipment.new(char_equipment_params)

    if @char_equipment.save
      render json: @char_equipment, status: :created, location: @char_equipment
    else
      render json: @char_equipment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_equipments/1
  def update
    if @char_equipment.update(char_equipment_params)
      render json: @char_equipment
    else
      render json: @char_equipment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_equipments/1
  def destroy
    @char_equipment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_equipment
      @char_equipment = CharEquipment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_equipment_params
      params.require(:char_equipment).permit(:type)
    end
end
