class MonstersController < ApplicationController
  before_action :set_monster, only: [:show, :update, :destroy]
  before_action :set_path, only: [:index]

  def index
    unless @path
      # GET /monsters
      @monsters = Monster.all
    else
      # GET /paths/:path_id/monsters
      @monsters = []
      monsters = @path.monsters

      monsters.each do |monster|
        m = monster.as_json
        m[:items] = monster.items
        m[:stats] = monster.stat

        skills = []
        monster.skills.each do |skill|
          sk = skill.as_json
          sp = skill.special.as_json
          fx = skill.special.effect
          unless fx.nil?
            sp[:effect] = fx.as_json
          end
          sk[:special] = sp
          skills << sk
        end
        m[:skills] = skills
        @monsters << m
      end
    end
    
    render json: @monsters
  end

  # GET /monsters/1
  def show
    render json: @monster
  end

  # POST /monsters
  def create
    @monster = Monster.new(monster_params)

    if @monster.save
      render json: @monster, status: :created, location: @monster
    else
      render json: @monster.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /monsters/1
  def update
    if @monster.update(monster_params)
      render json: @monster
    else
      render json: @monster.errors, status: :unprocessable_entity
    end
  end

  # DELETE /monsters/1
  def destroy
    @monster.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monster
      @monster = Monster.find(params[:id])
    end

    # Try to set current path
    def set_path
      if params.include? :path_id
        @path = Path.find params[:path_id]
      else
        @path = nil
      end
    end

    # Only allow a trusted parameter "white list" through.
    def monster_params
      params.require(:monster).permit(:name, :description, :stat_id)
    end
end
