class RelMonsterPathsController < ApplicationController
  before_action :set_rel_monster_path, only: [:show, :update, :destroy]

  # GET /rel_monster_paths
  def index
    @rel_monster_paths = RelMonsterPath.all

    render json: @rel_monster_paths
  end

  # GET /rel_monster_paths/1
  def show
    render json: @rel_monster_path
  end

  # POST /rel_monster_paths
  def create
    @rel_monster_path = RelMonsterPath.new(rel_monster_path_params)

    if @rel_monster_path.save
      render json: @rel_monster_path, status: :created, location: @rel_monster_path
    else
      render json: @rel_monster_path.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rel_monster_paths/1
  def update
    if @rel_monster_path.update(rel_monster_path_params)
      render json: @rel_monster_path
    else
      render json: @rel_monster_path.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rel_monster_paths/1
  def destroy
    @rel_monster_path.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rel_monster_path
      @rel_monster_path = RelMonsterPath.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rel_monster_path_params
      params.require(:rel_monster_path).permit(:path_id, :monster_id, :min_level, :max_level, :factor)
    end
end
