class BuffsController < ApplicationController
  before_action :set_buff, only: [:show, :update, :destroy]

  # GET /buffs
  def index
    @buffs = Buff.all

    render json: @buffs
  end

  # GET /buffs/1
  def show
    render json: @buff
  end

  # POST /buffs
  def create
    @buff = Buff.new(buff_params)

    if @buff.save
      render json: @buff, status: :created, location: @buff
    else
      render json: @buff.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /buffs/1
  def update
    if @buff.update(buff_params)
      render json: @buff
    else
      render json: @buff.errors, status: :unprocessable_entity
    end
  end

  # DELETE /buffs/1
  def destroy
    @buff.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_buff
      @buff = Buff.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def buff_params
      params.require(:buff).permit(:equipment_id, :stat, :amount)
    end
end
