class CharCurrentEquipmentsController < ApplicationController
  before_action :set_char_current_equipment, only: [:show, :update, :destroy]

  # GET /char_current_equipments
  def index
    @char_current_equipments = CharCurrentEquipment.all

    render json: @char_current_equipments
  end

  # GET /char_current_equipments/1
  def show
    render json: @char_current_equipment
  end

  # POST /char_current_equipments
  def create
    @char_current_equipment = CharCurrentEquipment.new(char_current_equipment_params)

    if @char_current_equipment.save
      render json: @char_current_equipment, status: :created, location: @char_current_equipment
    else
      render json: @char_current_equipment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_current_equipments/1
  def update
    if @char_current_equipment.update(char_current_equipment_params)
      render json: @char_current_equipment
    else
      render json: @char_current_equipment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_current_equipments/1
  def destroy
    @char_current_equipment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_current_equipment
      @char_current_equipment = CharCurrentEquipment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_current_equipment_params
      params.fetch(:char_current_equipment, {})
    end
end
