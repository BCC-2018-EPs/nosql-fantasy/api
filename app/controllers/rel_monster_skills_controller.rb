class RelMonsterSkillsController < ApplicationController
  before_action :set_rel_monster_skill, only: [:show, :update, :destroy]

  # GET /rel_monster_skills
  def index
    @rel_monster_skills = RelMonsterSkill.all

    render json: @rel_monster_skills
  end

  # GET /rel_monster_skills/1
  def show
    render json: @rel_monster_skill
  end

  # POST /rel_monster_skills
  def create
    @rel_monster_skill = RelMonsterSkill.new(rel_monster_skill_params)

    if @rel_monster_skill.save
      render json: @rel_monster_skill, status: :created, location: @rel_monster_skill
    else
      render json: @rel_monster_skill.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rel_monster_skills/1
  def update
    if @rel_monster_skill.update(rel_monster_skill_params)
      render json: @rel_monster_skill
    else
      render json: @rel_monster_skill.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rel_monster_skills/1
  def destroy
    @rel_monster_skill.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rel_monster_skill
      @rel_monster_skill = RelMonsterSkill.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rel_monster_skill_params
      params.require(:rel_monster_skill).permit(:monster_id, :skill_id)
    end
end
