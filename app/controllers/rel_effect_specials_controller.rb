class RelEffectSpecialsController < ApplicationController
  before_action :set_rel_effect_special, only: [:show, :update, :destroy]

  # GET /rel_effect_specials
  def index
    @rel_effect_specials = RelEffectSpecial.all

    render json: @rel_effect_specials
  end

  # GET /rel_effect_specials/1
  def show
    render json: @rel_effect_special
  end

  # POST /rel_effect_specials
  def create
    @rel_effect_special = RelEffectSpecial.new(rel_effect_special_params)

    if @rel_effect_special.save
      render json: @rel_effect_special, status: :created, location: @rel_effect_special
    else
      render json: @rel_effect_special.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rel_effect_specials/1
  def update
    if @rel_effect_special.update(rel_effect_special_params)
      render json: @rel_effect_special
    else
      render json: @rel_effect_special.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rel_effect_specials/1
  def destroy
    @rel_effect_special.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rel_effect_special
      @rel_effect_special = RelEffectSpecial.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rel_effect_special_params
      params.require(:rel_effect_special).permit(:chance, :effect, :special_id)
    end
end
