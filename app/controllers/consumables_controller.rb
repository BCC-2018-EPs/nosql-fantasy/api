class ConsumablesController < ApplicationController
  before_action :set_consumable, only: [:show, :update, :destroy]

  # GET /consumables
  def index
    @consumables = Consumable.all

    render json: @consumables
  end

  # GET /consumables/1
  def show
    render json: @consumable
  end

  # POST /consumables
  def create
    @consumable = Consumable.new(consumable_params)

    if @consumable.save
      render json: @consumable, status: :created, location: @consumable
    else
      render json: @consumable.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /consumables/1
  def update
    if @consumable.update(consumable_params)
      render json: @consumable
    else
      render json: @consumable.errors, status: :unprocessable_entity
    end
  end

  # DELETE /consumables/1
  def destroy
    @consumable.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_consumable
      @consumable = Consumable.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def consumable_params
      params.require(:consumable).permit(:item_id, :special_id)
    end
end
