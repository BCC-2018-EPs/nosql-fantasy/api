class RelItemMonstersController < ApplicationController
  before_action :set_rel_item_monster, only: [:show, :update, :destroy]

  # GET /rel_item_monsters
  def index
    @rel_item_monsters = RelItemMonster.all

    render json: @rel_item_monsters
  end

  # GET /rel_item_monsters/1
  def show
    render json: @rel_item_monster
  end

  # POST /rel_item_monsters
  def create
    @rel_item_monster = RelItemMonster.new(rel_item_monster_params)

    if @rel_item_monster.save
      render json: @rel_item_monster, status: :created, location: @rel_item_monster
    else
      render json: @rel_item_monster.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rel_item_monsters/1
  def update
    if @rel_item_monster.update(rel_item_monster_params)
      render json: @rel_item_monster
    else
      render json: @rel_item_monster.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rel_item_monsters/1
  def destroy
    @rel_item_monster.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rel_item_monster
      @rel_item_monster = RelItemMonster.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rel_item_monster_params
      params.require(:rel_item_monster).permit(:item_id, :monster_id)
    end
end
