class GameClassesController < ApplicationController
  before_action :set_game_class, only: [:show, :update, :destroy]

  # GET /game_classes
  def index
    @game_classes = GameClass.all

    render json: @game_classes
  end

  # GET /game_classes/1
  def show
    render json: @game_class
  end

  # POST /game_classes
  def create
    @game_class = GameClass.new(game_class_params)

    if @game_class.save
      render json: @game_class, status: :created, location: @game_class
    else
      render json: @game_class.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /game_classes/1
  def update
    if @game_class.update(game_class_params)
      render json: @game_class
    else
      render json: @game_class.errors, status: :unprocessable_entity
    end
  end

  # DELETE /game_classes/1
  def destroy
    @game_class.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_class
      @game_class = GameClass.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def game_class_params
      params.require(:game_class).permit(:base_id, :growth_id, :name)
    end
end
