class SpecialsController < ApplicationController
  before_action :set_special, only: [:show, :update, :destroy]

  # GET /specials
  def index
    @specials = Special.all

    render json: @specials
  end

  # GET /specials/1
  def show
    render json: @special
  end

  # POST /specials
  def create
    @special = Special.new(special_params)

    if @special.save
      render json: @special, status: :created, location: @special
    else
      render json: @special.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /specials/1
  def update
    if @special.update(special_params)
      render json: @special
    else
      render json: @special.errors, status: :unprocessable_entity
    end
  end

  # DELETE /specials/1
  def destroy
    @special.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_special
      @special = Special.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def special_params
      params.require(:special).permit(:target_self, :power, :stat)
    end
end
