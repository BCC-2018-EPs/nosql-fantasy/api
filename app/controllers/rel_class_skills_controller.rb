class RelClassSkillsController < ApplicationController
  before_action :set_rel_class_skill, only: [:show, :update, :destroy]

  # GET /rel_class_skills
  def index
    @rel_class_skills = RelClassSkill.all

    render json: @rel_class_skills
  end

  # GET /rel_class_skills/1
  def show
    render json: @rel_class_skill
  end

  # POST /rel_class_skills
  def create
    @rel_class_skill = RelClassSkill.new(rel_class_skill_params)

    if @rel_class_skill.save
      render json: @rel_class_skill, status: :created, location: @rel_class_skill
    else
      render json: @rel_class_skill.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rel_class_skills/1
  def update
    if @rel_class_skill.update(rel_class_skill_params)
      render json: @rel_class_skill
    else
      render json: @rel_class_skill.errors, status: :unprocessable_entity
    end
  end

  # DELETE /rel_class_skills/1
  def destroy
    @rel_class_skill.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rel_class_skill
      @rel_class_skill = RelClassSkill.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rel_class_skill_params
      params.require(:rel_class_skill).permit(:game_class_id, :skill_id, :level)
    end
end
