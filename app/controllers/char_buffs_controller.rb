class CharBuffsController < ApplicationController
  before_action :set_char_buff, only: [:show, :update, :destroy]

  # GET /char_buffs
  def index
    @char_buffs = CharBuff.all

    render json: @char_buffs
  end

  # GET /char_buffs/1
  def show
    render json: @char_buff
  end

  # POST /char_buffs
  def create
    @char_buff = CharBuff.new(char_buff_params)

    if @char_buff.save
      render json: @char_buff, status: :created, location: @char_buff
    else
      render json: @char_buff.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_buffs/1
  def update
    if @char_buff.update(char_buff_params)
      render json: @char_buff
    else
      render json: @char_buff.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_buffs/1
  def destroy
    @char_buff.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_buff
      @char_buff = CharBuff.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_buff_params
      params.require(:char_buff).permit(:stat, :amount)
    end
end
