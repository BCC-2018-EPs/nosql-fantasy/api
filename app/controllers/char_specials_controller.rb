class CharSpecialsController < ApplicationController
  before_action :set_char_special, only: [:show, :update, :destroy]

  # GET /char_specials
  def index
    @char_specials = CharSpecial.all

    render json: @char_specials
  end

  # GET /char_specials/1
  def show
    render json: @char_special
  end

  # POST /char_specials
  def create
    @char_special = CharSpecial.new(char_special_params)

    if @char_special.save
      render json: @char_special, status: :created, location: @char_special
    else
      render json: @char_special.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_specials/1
  def update
    if @char_special.update(char_special_params)
      render json: @char_special
    else
      render json: @char_special.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_specials/1
  def destroy
    @char_special.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_special
      @char_special = CharSpecial.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_special_params
      params.require(:char_special).permit(:power, :target_self, :stat)
    end
end
