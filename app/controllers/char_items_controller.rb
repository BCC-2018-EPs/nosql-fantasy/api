class CharItemsController < ApplicationController
  before_action :set_char_item, only: [:show, :update, :destroy]

  # GET /char_items
  def index
    @char_items = CharItem.all

    render json: @char_items
  end

  # GET /char_items/1
  def show
    render json: @char_item
  end

  # POST /char_items
  def create
    @char_item = CharItem.new(char_item_params)

    if @char_item.save
      render json: @char_item, status: :created, location: @char_item
    else
      render json: @char_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_items/1
  def update
    if @char_item.update(char_item_params)
      render json: @char_item
    else
      render json: @char_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_items/1
  def destroy
    @char_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_item
      @char_item = CharItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_item_params
      params.require(:char_item).permit(:name, :cost)
    end
end
