class CharSkillsController < ApplicationController
  before_action :set_char_skill, only: [:show, :update, :destroy]

  # GET /char_skills
  def index
    @char_skills = CharSkill.all

    render json: @char_skills
  end

  # GET /char_skills/1
  def show
    render json: @char_skill
  end

  # POST /char_skills
  def create
    @char_skill = CharSkill.new(char_skill_params)

    if @char_skill.save
      render json: @char_skill, status: :created, location: @char_skill
    else
      render json: @char_skill.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_skills/1
  def update
    if @char_skill.update(char_skill_params)
      render json: @char_skill
    else
      render json: @char_skill.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_skills/1
  def destroy
    @char_skill.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_skill
      @char_skill = CharSkill.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_skill_params
      params.require(:char_skill).permit(:name, :accuracy, :cost, :multiplier_type)
    end
end
