class CharactersController < ApplicationController
  before_action :set_user
  before_action :set_character, only: [:show, :update, :destroy]

  # GET /characters
  def index
    @characters = @user.characters

    render json: @characters
  end

  # GET /characters/1
  def show
    render json: @character
  end

  # POST /characters
  def create
    # @character = @user.characters.create!(character_params)

    @character = @user.characters.new character_params
    st = GameClass.find(params[:character][:game_class_id]).base

    cs = CharStat.new
    cs.hp,  cs.mp,  cs.ap   = st.hp,  st.mp,  st.ap
    cs.str, cs.def, cs.int  = st.str, st.def, st.int
    cs.agi, cs.dex, cs.luk  = st.agi, st.dex, st.luk

    @character.char_stat = cs

    @character.curr_hp, @character.curr_mp, @character.curr_ap = st.hp, st.mp, st.ap

    relations = RelClassSkill.where("game_class_id = ? and level <= ?", params[:character][:game_class_id],1)

    @character.char_skills = []

    relations.each do |rel|
      sk = rel.skill
      s = CharSkill.new
      s.name, s.accuracy = sk.name, sk.accuracy
      s.cost_type, s.cost_amount = sk.cost_type, sk.cost_amount
      s.ref_stat = sk.ref_stat

      rsp = sk.special
      sp = CharSpecial.new
      sp.power, sp.stat = rsp.power, rsp.stat
      sp.target_self = rsp.target_self

      s.skill_special = sp

      @character.char_skills << s
    end

    @character.char_items = []

    @character.char_current_equipment = CharCurrentEquipment.new

    @user.save

    render json: @user
    # @character = Character.new(character_params)
    #if @character.save
    #  render json: @character, status: :created, location: @character
    #else
    #  render json: @character.errors, status: :unprocessable_entity
    #end
  end

  # PATCH/PUT /characters/1
  def update
    if @character.update(character_params)
      render json: @character
    else
      render json: @character.errors, status: :unprocessable_entity
    end
  end

  # DELETE /characters/1
  def destroy
    @character.destroy
  end

  private

    def set_user
      @user = User.find(params[:user_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_character
      self.set_user unless @user
      @character = @user.characters.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def character_params
      params.require(:character).permit(:name, :game_class_id, :level, :gold, :exp, :curr_hp, :curr_mp, :curr_ap, :curr_location_id)
    end
end
