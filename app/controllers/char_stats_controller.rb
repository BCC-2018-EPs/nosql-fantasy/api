class CharStatsController < ApplicationController
  before_action :set_char_stat, only: [:show, :update, :destroy]

  # GET /char_stats
  def index
    @char_stats = CharStat.all

    render json: @char_stats
  end

  # GET /char_stats/1
  def show
    render json: @char_stat
  end

  # POST /char_stats
  def create
    @char_stat = CharStat.new(char_stat_params)

    if @char_stat.save
      render json: @char_stat, status: :created, location: @char_stat
    else
      render json: @char_stat.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /char_stats/1
  def update
    if @char_stat.update(char_stat_params)
      render json: @char_stat
    else
      render json: @char_stat.errors, status: :unprocessable_entity
    end
  end

  # DELETE /char_stats/1
  def destroy
    @char_stat.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_char_stat
      @char_stat = CharStat.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def char_stat_params
      params.require(:char_stat).permit(:hp, :mp, :ap, :str, :agi, :dex, :int, :def, :luk)
    end
end
