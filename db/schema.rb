# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180627234717) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "buffs", force: :cascade do |t|
    t.bigint "equipment_id"
    t.integer "stat"
    t.integer "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["equipment_id", "stat"], name: "index_buffs_on_equipment_id_and_stat", unique: true
    t.index ["equipment_id"], name: "index_buffs_on_equipment_id"
  end

  create_table "consumables", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "special_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id", "special_id"], name: "index_consumables_on_item_id_and_special_id", unique: true
    t.index ["item_id"], name: "index_consumables_on_item_id"
    t.index ["special_id"], name: "index_consumables_on_special_id"
  end

  create_table "effects", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_effects_on_name", unique: true
  end

  create_table "equipment", force: :cascade do |t|
    t.bigint "item_id"
    t.integer "body_slot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id", "body_slot"], name: "index_equipment_on_item_id_and_body_slot", unique: true
    t.index ["item_id"], name: "index_equipment_on_item_id"
  end

  create_table "game_classes", force: :cascade do |t|
    t.bigint "base_id"
    t.bigint "growth_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["base_id"], name: "index_game_classes_on_base_id"
    t.index ["growth_id"], name: "index_game_classes_on_growth_id"
    t.index ["name"], name: "index_game_classes_on_name", unique: true
  end

  create_table "items", force: :cascade do |t|
    t.string "name"
    t.decimal "cost"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_items_on_name", unique: true
  end

  create_table "locations", force: :cascade do |t|
    t.text "description"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_locations_on_name", unique: true
  end

  create_table "monsters", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_monsters_on_name", unique: true
    t.index ["stat_id"], name: "index_monsters_on_stat_id"
  end

  create_table "paths", force: :cascade do |t|
    t.bigint "departure_id"
    t.bigint "destination_id"
    t.integer "length"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departure_id", "destination_id"], name: "index_paths_on_departure_id_and_destination_id", unique: true
    t.index ["departure_id"], name: "index_paths_on_departure_id"
    t.index ["destination_id"], name: "index_paths_on_destination_id"
  end

  create_table "rel_class_skills", force: :cascade do |t|
    t.bigint "game_class_id"
    t.bigint "skill_id"
    t.integer "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_class_id"], name: "index_rel_class_skills_on_game_class_id"
    t.index ["skill_id"], name: "index_rel_class_skills_on_skill_id"
  end

  create_table "rel_effect_specials", force: :cascade do |t|
    t.decimal "chance"
    t.bigint "effect_id"
    t.bigint "special_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["effect_id", "special_id"], name: "effect_special_unique_key", unique: true
    t.index ["effect_id"], name: "index_rel_effect_specials_on_effect_id"
    t.index ["special_id"], name: "index_rel_effect_specials_on_special_id"
  end

  create_table "rel_item_monsters", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "monster_id"
    t.decimal "chance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_id"], name: "index_rel_item_monsters_on_item_id"
    t.index ["monster_id"], name: "index_rel_item_monsters_on_monster_id"
  end

  create_table "rel_monster_paths", force: :cascade do |t|
    t.bigint "path_id"
    t.bigint "monster_id"
    t.integer "min_level"
    t.integer "max_level"
    t.integer "factor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["monster_id"], name: "index_rel_monster_paths_on_monster_id"
    t.index ["path_id"], name: "index_rel_monster_paths_on_path_id"
  end

  create_table "rel_monster_skills", force: :cascade do |t|
    t.bigint "monster_id"
    t.bigint "skill_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["monster_id"], name: "index_rel_monster_skills_on_monster_id"
    t.index ["skill_id"], name: "index_rel_monster_skills_on_skill_id"
  end

  create_table "skills", force: :cascade do |t|
    t.string "name"
    t.bigint "special_id"
    t.integer "accuracy"
    t.integer "cost_amount"
    t.integer "cost_type"
    t.integer "ref_stat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_skills_on_name", unique: true
    t.index ["special_id"], name: "index_skills_on_special_id"
  end

  create_table "specials", force: :cascade do |t|
    t.boolean "target_self", default: false
    t.integer "power"
    t.integer "stat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stats", force: :cascade do |t|
    t.integer "hp"
    t.integer "mp"
    t.integer "ap"
    t.integer "str"
    t.integer "agi"
    t.integer "dex"
    t.integer "int"
    t.integer "def"
    t.integer "luk"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hp", "mp", "ap", "str", "agi", "dex", "int", "def", "luk"], name: "stats_unique_index", unique: true
  end

  add_foreign_key "buffs", "equipment"
  add_foreign_key "consumables", "items"
  add_foreign_key "consumables", "specials"
  add_foreign_key "equipment", "items"
  add_foreign_key "monsters", "stats"
  add_foreign_key "rel_class_skills", "game_classes"
  add_foreign_key "rel_class_skills", "skills"
  add_foreign_key "rel_effect_specials", "specials"
  add_foreign_key "rel_item_monsters", "items"
  add_foreign_key "rel_item_monsters", "monsters"
  add_foreign_key "rel_monster_paths", "monsters"
  add_foreign_key "rel_monster_paths", "paths"
  add_foreign_key "rel_monster_skills", "monsters"
  add_foreign_key "rel_monster_skills", "skills"
  add_foreign_key "skills", "specials"
end
