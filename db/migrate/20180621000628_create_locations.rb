class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.text :description
      t.string :name
      t.index :name, unique: true

      t.timestamps
    end
  end
end
