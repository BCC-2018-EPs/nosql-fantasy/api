class CreatePaths < ActiveRecord::Migration[5.1]
  def change
    create_table :paths do |t|
      t.references :departure#, foreign_key: true
      t.references :destination#, foreign_key: true
      t.integer :length

      t.timestamps
    end
    add_index :paths,
      [:departure_id, :destination_id],
      unique: true
  end
end
