class CreateStats < ActiveRecord::Migration[5.1]
  def change
    create_table :stats do |t|
      t.integer :hp
      t.integer :mp
      t.integer :ap
      t.integer :str
      t.integer :agi
      t.integer :dex
      t.integer :int
      t.integer :def
      t.integer :luk

      t.timestamps
    end
    add_index :stats,
      [:hp, :mp, :ap, :str, :agi, :dex, :int, :def, :luk],
      unique: true, name: 'stats_unique_index'
  end
end
