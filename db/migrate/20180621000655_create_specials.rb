class CreateSpecials < ActiveRecord::Migration[5.1]
  def change
    create_table :specials do |t|
      t.boolean :target_self, default: false
      t.integer :power
      t.integer :stat

      t.timestamps
    end
  end
end
