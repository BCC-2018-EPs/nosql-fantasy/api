class CreateSkills < ActiveRecord::Migration[5.1]
  def change
    create_table :skills do |t|
      t.string :name
      t.references :special, foreign_key: true
      t.integer :accuracy
      t.integer :cost_amount
      t.integer :cost_type
      t.integer :ref_stat

      t.index :name, unique: true

      t.timestamps
    end
  end
end
