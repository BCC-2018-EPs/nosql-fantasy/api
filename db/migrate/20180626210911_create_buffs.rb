class CreateBuffs < ActiveRecord::Migration[5.1]
  def change
    create_table :buffs do |t|
      t.references :equipment, foreign_key: true
      t.integer :stat
      t.integer :amount

      t.timestamps
    end
    add_index :buffs,
      [:equipment_id, :stat],
      unique: true
  end
end
