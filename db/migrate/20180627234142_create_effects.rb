class CreateEffects < ActiveRecord::Migration[5.1]
  def change
    create_table :effects do |t|
      t.string :name
      t.index :name, unique: true

      t.timestamps
    end
  end
end
