class CreateRelEffectSpecials < ActiveRecord::Migration[5.1]
  def change
    create_table :rel_effect_specials do |t|
      t.decimal :chance
      t.references :effect
      t.references :special, foreign_key: true

      t.timestamps
    end
    add_index :rel_effect_specials,
      [:effect_id, :special_id],
      unique: true, name: 'effect_special_unique_key'
  end
end
