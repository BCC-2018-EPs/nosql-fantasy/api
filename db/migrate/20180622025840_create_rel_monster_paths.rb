class CreateRelMonsterPaths < ActiveRecord::Migration[5.1]
  def change
    create_table :rel_monster_paths do |t|
      t.references :path, foreign_key: true
      t.references :monster, foreign_key: true
      t.integer :min_level
      t.integer :max_level
      t.integer :factor

      t.timestamps
    end
  end
end
