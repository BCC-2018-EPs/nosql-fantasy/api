class CreateEquipment < ActiveRecord::Migration[5.1]
  def change
    create_table :equipment do |t|
      t.references :item, foreign_key: true
      t.integer :body_slot

      t.timestamps
    end
    add_index :equipment,
      [:item_id, :body_slot],
      unique: true
  end
end
