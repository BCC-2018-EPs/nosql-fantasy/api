class CreateRelClassSkills < ActiveRecord::Migration[5.1]
  def change
    create_table :rel_class_skills do |t|
      t.references :game_class, foreign_key: true
      t.references :skill, foreign_key: true
      t.integer :level

      t.timestamps
    end
  end
end
