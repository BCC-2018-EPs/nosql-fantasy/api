class CreateGameClasses < ActiveRecord::Migration[5.1]
  def change
    create_table :game_classes do |t|
      t.references :base
      t.references :growth
      t.string :name

      t.index :name, unique: true

      t.timestamps
    end
  end
end
