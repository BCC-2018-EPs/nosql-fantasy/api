class CreateRelItemMonsters < ActiveRecord::Migration[5.1]
  def change
    create_table :rel_item_monsters do |t|
      t.references :item, foreign_key: true
      t.references :monster, foreign_key: true
      t.decimal :chance

      t.timestamps
    end
  end
end
