class CreateConsumables < ActiveRecord::Migration[5.1]
  def change
    create_table :consumables do |t|
      t.references :item, foreign_key: true
      t.references :special, foreign_key: true

      t.timestamps
    end
    add_index :consumables,
      [:item_id, :special_id],
      unique: true
  end
end
