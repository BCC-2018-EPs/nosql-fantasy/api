class CreateMonsters < ActiveRecord::Migration[5.1]
  def change
    create_table :monsters do |t|
      t.string :name
      t.text :description
      t.references :stat, foreign_key: true

      t.index :name, unique: true

      t.timestamps
    end
  end
end
