class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.decimal :cost
      t.text :description

      t.index :name, unique: true

      t.timestamps
    end
  end
end
