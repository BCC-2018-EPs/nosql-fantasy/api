class CreateRelMonsterSkills < ActiveRecord::Migration[5.1]
  def change
    create_table :rel_monster_skills do |t|
      t.references :monster, foreign_key: true
      t.references :skill, foreign_key: true

      t.timestamps
    end
  end
end
