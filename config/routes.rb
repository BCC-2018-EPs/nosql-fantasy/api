Rails.application.routes.draw do
  # mongodb
  resources :users do
    resources :characters
  end

  # postgresql
  resources :paths do
    get '/monsters', to: 'monsters#index'
  end

  resources :buffs
  resources :effects
  resources :game_classes
  resources :monsters
  resources :skills
  resources :consumables
  resources :equipment
  resources :items
  resources :locations
  resources :specials
  resources :stats
  resources :rel_monster_paths
  resources :rel_item_monsters
  resources :rel_class_skills
  resources :rel_monster_skills
  resources :rel_effect_specials

  post '/login', to: 'users#login'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
